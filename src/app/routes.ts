import { HomeComponent } from "./home/home.component";
import { DetailsComponent } from "./details/details.component";
import { Routes } from '@angular/router';

const routeConfig: Routes = [
    {
        path: '',
        component: HomeComponent,
        title: 'Home Page'
    },
    {
        path: 'details/:id',
        component: DetailsComponent,
        title: 'Home Details'
    }
];

export default routeConfig;
